@extends('master')
@section('content')
    <div class="row results">
        @if($artists || $artworks)
            <div class="col-lg-offset-2 col-lg-8">
                <h1>Search results for: {{$query}}</h1>
            </div>
            @if($artists)
                <div class="col-lg-offset-2 col-lg-8">

                    <h2>Artists</h2>
                    @foreach($artists as $artist)
                        <a href="/artist/{{$artist->id}}">{{$artist->name}}</a>
                    @endforeach
                </div>
            @endif
            @if($artworks)
                <div class="col-lg-offset-2 col-lg-8">
                    <h2>Artworks</h2>
                    @foreach($artworks as $artwork)
                        <a href="/art/{{$artwork->id}}">{{$artwork->name}}</a>
                    @endforeach
                </div>
            @endif
        @else
            <div class="text-center center-block no-results">
                <h1>Sorry we couldn't find anything...</h1>
            </div>
        @endif
    </div>
@endsection